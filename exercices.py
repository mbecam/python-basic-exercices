"""
Exercices.

Implémentez les méthodes ci-dessous.
Pour executer vos tests il vous faudra utiliser pytest
"""


def htc_to_ttc(htc_cost: float, discount_rate: float = 0) -> float :
    """
    Exercice 1 :
    Calcule le coût TTC d'un produit.
    discount_rate : taux de réduction compris entre 0 et 1
    Taux de taxes : 20.6 %
    Retourne un float arrondi à deux décimales
    """
    ttc_cost = htc_cost*1.206
    if discount_rate < 0 or discount_rate > 1:
        raise Exception("Discount rate must be a number between 0 and 1")
    else:
        ttc_cost *= 1 - discount_rate
    return float("%.2f" % ttc_cost)


def divisors(value: int = 0):
    """
    Exercice 2 :
    A partir d'un nombre donné,
    retourne un set de ses diviseurs (sans répétition)
    s’il y en a, ou « PREMIER » s’il n’y en a pas.
    """

    is_prime = True
    all_divisors = set()

    if value > 1:
        for i in range(2, value):
            if not value % i: # if i is a divisor of value
                is_prime = False
                all_divisors.add(i)
    else:  # integers inferior or equal to 1 are not prime numbers
        is_prime = False
        all_divisors.clear()

    prime_divisors = all_divisors.copy()

    # remove non prime divisors
    for divisor in all_divisors:
        if divisors(divisor) != "PREMIER":
            prime_divisors.discard(divisor)

    if is_prime:
        return "PREMIER"
    else:
        return prime_divisors
